---
title: "Homepage"
date: 2023-02-16
---
# HELLO 👋

**Welcome to my personal website**

I am Daniel Corona, a Ph.D candidate at the Institute of Geophysics at National Autonomous University of Mexico ([UNAM_MX](https://twitter.com/UNAM_MX)).

Some people say I am a Hobbit and not-so-teenage mutant ninja turtle!🐢
I love almost all **music**, rock, pop, metal, classic, celta, regional, etc 🎧🎧🎧.
I am also passionate about fiction and fantasy **books**.
[PumasMX](https://twitter.com/PumasMX) soccer & [Steelers](https://twitter.com/steelers) football.
I am also a Geoscience teacher.
I like code and learning new developments in python **Python** 💻.

I am a science communicator with [SeismoIsTheWave](https://twitter.com/seismoisthewave), an initiative where we publish seismology content, participate in geoscience workshops and give regular lectures on seismology.
