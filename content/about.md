---
title: About my Research
---


Let me tell you a little about myself and my profile.

My work is about the study of historical earthquakes. I have obtained hypocentral locations (through a Bayesian inversion) and focal mechanisms (using an ad-hoc code based on Genetic Algorithms), for historical earthquakes based on on legacy seismic data.  

I also have advanced experience in **scientific programming**, mainly in implementing methodologies and efficiently solving physical mathematical problems, inversion codes, and database processing.  
In fact I developed a [**GUI**](https://zenodo.org/record/6272823) for analog seismogram processing and analysis.  

I also have experience in seismic monitoring networks installation and maintenance, databases processing and analysis, and a little bit in probabilistic assessment of seismic hazard **(PASH)**.  

I am a geoscience lecture teacher and science communicator.  

---

---

# Research Interest

+ I have an interest in expanding and combining the research I have developed and participated in, with nowadays novel research lines, and in directly linking my research to seismic hazard studies.  

+ Also to create ***Historical earthquakes*** data bases. (Metadata and /or seismograms ) towards **Historical Earthquakes** reanalysis and the re-use and preservation of legacy seismic

+ My research interests include eartquake source analysis, computational seismology and automatation process

+ Among another interest I have in new research lines, such as acquisition and processing of
Distributed Acoustic Sensing (DAS) data.  

+ I would like to apply machine learning methodologies (of which I know and understand) for the processing of large databases in seismology and/or for automation of detection and processing processes.  

---

---

<div class="views">
    <span class="views">
        <img src="https://visitor-badge.glitch.me/badge?page_id={{ .Permalink }}" alt="Views"/>
    </span>
</div>
