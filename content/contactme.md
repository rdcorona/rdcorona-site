+++
title = "Contact"
slug = "contact"
+++

 * ![](/img/icons8-email-100.png) Email:  [geofisica.corona@gmail.com](mailto:geofisica.corona@gmail.com) 

* ![](/img/icons8-email-100.png) Institutional-Email: [rdcorona@igeofisica.unam.mx](mailto:rdcorona@igeofisica.unam.mx)
<!-- * ![](/img/icons8-telegram-app-96.png)   Phone:  +525534895612 ![](/img/icons8-whatsapp.gif) -->


---
___

# Social

 + <img src="/img/icons8-twitter.gif" alt="twitter" width="50" height="50"> [@rdcoronaf](https://twitter.com/rdcoronaf)
 + <img src="/img/gitlab-logo.png" alt="gitlab" width="50" height="50"> [@rdcorona](https://gitlab.com/rdcorona)
 + <img src="/img/icons8-linkedin-48.png" alt="Linkedin" width="50" height="50"> [Raul Daniel Corona-Fernandez](https://www.linkedin.com/in/raúl-daniel-corona-fernández-36aab677/)

