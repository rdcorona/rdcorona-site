+++
title = "Research Projects"
slug = "projects"
+++



#### **Wave propagation modeling in metamaterials**
Study of seismic wave propagation behavior in metamaterials to generate frequency gapsfor seismic engineering. Proyect by the UNAM Institute of Engineerin and Texas U.

---
---

#### Identification of the currently regions of greatest danger for the Federal Highway Network and in the future due to the effects of Climate Change.

External consultant as a seismology expert. Working on a Probabilistic assessment of seismic hazard (PASH) for the Federal Highway Network in Mexico.

---
---

#### **Repeating Large Earthquakes (M≥7.0) along the Mexican Subduction Zone.**

 We are carry out a qualitative and quantitative waveform comparison for different historical and relatively recent earthquakes in Mexico, to establish repetition and to estimated return periods.

---
---

#### Re-examination of the 1928 El Parral, Mexico earthquake (M6.3) using a new multiplatform graphical vectorization and correction software for legacy seismic data.

Modern analysis of earthquakes depends on digital timeseries; however, **only about 30% of the timespan of recorded seismicity is available in digital format**. During the first half of the 20th century, most of the earthquake ground motions in Mexico were recorded by Wiechert mechanical instruments on smoked paper. In this work we developed and use **Tiitba**, a new portable multi-platform graphical user interface (GUI) open-source software coded on `python`, **to vectorize and correct old analog seismograms**. Using this software, we vectorized the 11/1/1928 El Parral (M6.3) earthquake seismograms to obtain the constant time-interval digitized time series for each component and station of the available seismograms. Then, we obtained its source focal mechanism using a genetic algorithm methodology. Also, with an auxiliary Tiitba module, we constructed a SEISAN S-file to relocate the hypocenter of this earthquake. The obtained relocation is about 125km south of a recently recorded seismic swarm occurred in 2013, that presents consistent source parameters regarding the focal mechanism obtained in this work.

---
---

#### Re-Analysis of large earthquakes in Mexico from the first half of the 20th century through legacy seismic data

In this project we want to recovery and to preserve legacy analog seismograms for large earthquakes in Mexico during the first half of the 20th century. I have developed a graphical user interface GUI to vectorize, analyzed and correct analog seismograms from early 20th century to taking them from paper into evenly spaced digital time-series.
This re-analysis include hypocenter relocations by two different methodologys: A Bayesian inversion and with a  SEISAN S-file. Also, a focal mechanism determination trough a Genetic Algorithm methodology

---
---

#### TIITBA

Tiitba is the prehispanic Mayan word for **earthquake **.  
[**Tiitba**](https://zenodo.org/record/6272823#.Y-1hLq9BxhE) is a **GUI** for Historical Seismograms Vectorization Analysis and Correction

[![Tiitba](/img/logo.png)](https://zenodo.org/record/6272823#.Y-1hLq9BxhE)

- Abstract

> Modern analysis of earthquakes depends on digital time-series, however only about 25% of the recorded seismicity is available in digital format. Most of the earthquake ground-motions were recorded by mechanical instruments on smoked paper, photographic film, or ink on a whitepaper up to early 1970. We present Tiitba-HiSVAC, a new portable multi-platform graphical user interface (GUI) open-source software coded on python, to vectorize analog seismograms of historical earthquakes, particularly but not limited, for those recorded on smoked paper.
> ![tiitba-main](/img/MainW.png "TiitbaC main window")

---
---

#### Project CONACYT PN-15-639 

To calculate dynamic deformations with a nearby station arrangement.  
**Installation and maintenance** of a semi-permanent 3 seismic stations network with Trillium 120 Posthole instruments in Zihuatanejo, Guerrero airport

---

---
