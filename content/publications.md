+++
title = "Publications chronologically"
slug = "publications"
+++

### Publications on Journals

<ol type="1" reversed>
 	<li>Singh, S. K., Iglesias Mendoza, A., Arroyo, D., Pérez-Campos, X., Ordaz, M., Mendoza, C., <b>Corona-Fernández, R. D.</b>, Espíndola, V. H., González-Ávila, D., Martínez-López, R., Castro-Artola, O., Santoyo, M. A., & Franco, S. I. <b>(2023).</b> <i>A Seismological Study of the Michoacán-Colima, Mexico, Earthquake of 19 September 2022 (Mw7.6).</i>, Geofísica Internacional 62(2), 445–465. <a href="https://doi.org/10.22201/igeof.2954436xe.2023.62.2.1453">[Doi.org/10.22201/igeof.2954436xe.2023.62.2.1453]</a></li>
 	<li>Allen Husker, Maximilian J. Werner, José A. Bayona, Miguel Santoyo, <b>Raul Daniel Corona-Fernandez, 2022</b>, <i>A Test of the Earthquake Gap Hypothesis in Mexico: the case of the Guerrero Gap.</i>, Bulletin of the Seismological Society of America.<a href="https://pubs.geoscienceworld.org/ssa/bssa/article-abstract/113/1/468/619098/A-Test-of-the-Earthquake-Gap-Hypothesis-in-Mexico?redirectedFrom=fulltext">[Doi.org/10.1785/0120220094]</a></li>
 	<li>Arturo Iglesias, Shri K. Singh, Oscar Castro, Xyoli Pérez-Campos, <b>Raul D. CoronaFernandez</b>, Miguel A. Santoyo, Víctor H. Espíndola, Danny Arroyo, Sara I. Franco,<b>2022</b>, <i>A Source Study of the Acapulco, Mexico Earthquake of 8 September 2021 (Mw 7.0)</i>, Seismological Research Letters.<a href="https://pubs.geoscienceworld.org/ssa/srl/article-abstract/93/6/3205/616605/A-Source-Study-of-the-Mw-7-0-Acapulco-Mexico?redirectedFrom=fulltext">[Doi.org/10.1785/0220220124]</a></li>
 	<li><b>Corona-Fernandez, R.D.</b> & Santoyo, M.A., <b>2022</b>, <i>Re-examination of the 1928 Parral, Mexico earthquake (M6.3) using a new multiplatform graphical vectorization and correction software for legacy seismic data</i>, Geoscience Data Journal, 10, 178–192. Available from: <a href="https://rmets.onlinelibrary.wiley.com/doi/full/10.1002/gdj3.159">[Doi.org/10.1002/gdj3.159]</a></li>
	<li>Sawires, R., Santoyo, M.A., Peláez, J.A., <b>Corona-Fernández, R.D.</b>, <b>2019</b>, <i>An updated and unified earthquake catalog from 1787 to 2018 for seismic hazard assessment studies in Mexico</i>. Sci Data 6, 241 <a href="https://www.nature.com/articles/s41597-019-0234-z">[Doi:10.1038/s41597-019-0234-z]</a></li>
</ol>

### Accepted or on revision Journal articles 

### Others Publications

<ol type="1" reversed>
 	<li> Shri Krishna Singh, <b>Corona-Fernandez, R.D.</b>, Miguel Angel Santoyo García Galiano, Arturo Iglesias, <b>in preparation</b>,<i> Repeating Large Earthquakes (M≥7.0) along the Mexican Subduction Zone</i>
	<li>Aguilar-Velázquez, M.J., <b>Corona-Fernández, R.D.</b>, Rodríguez-Domínguez, M.Á, <b>2022</b>, Is September really “earthquake month” in Mexico?, Temblor, <a href="http://doi.org/10.32858/temblor.285">[Doi.org/10.32858/temblor.285]</a></li> 
	<li><b>Corona-Fernandez, R.D.</b> & Santoyo, M.A., <b>2022</b>, TIITBA - a graphical interface for historical seismograms, vectorization, analysis and, correction, Zenodo repocitory. <b>Available from:</b> <a href="https://zenodo.org/record/6272823#.YmwEpM5BwW0">[Doi: 10.5281/zenodo.6272823]</a></li>
</ol>

---

---

# Conference Proceeding chronologically

<ol type="1" reversed>
	<li><b>Raul Daniel Corona Fernandez</b>, Shri Krishna Singh, Miguel Angel Santoyo García Galiano, Arturo Iglesias, <i>A focal mechanism analysis for the 1928 Parral (M=6.3) and OAXACA (M=7.5) earthquake sequence, Mexico, based on analog smoked paper seismograms,</i> <b>LACSC-2022</b> Quito, Ecuador.</li>
	<li><b>Geosciences Center (CEGEO), UNAM. Institutional Seminar</b>.  <i>Old data for new knowledge: The use of analog seismograms to re-analyze large earthquakes in Mexico during the first half of the 20th century,</i> <b>January- 2022</b></li>
	<li><b>Raul Daniel Corona Fernandez</b>, Shri Krishna Singh, Miguel Angel Santoyo García Galiano, <i>Is the Acapulco earthquake of 8 september 2021 (mw 7.0) a repeat of the 11 May 1962 earthquake?,</i> <b>2021-UGM</b>Annual Meeting, Mexico.</li>
	<li><b>Raul Daniel Corona Fernandez</b>, Miguel Angel Santoyo García Galiano, <i>The 1928 Oaxaca, Mexico large-earthquakes sequence (M6.5-M7.8): preliminary results from the re-analysis of legacy seismograms and seismic bulletins information,</i> <b>37th General Assembly of the European Seismological Commission, 2021</b> Athens.</li>
	<li><b>Raul Daniel Corona Fernandez</b>, Miguel Angel Santoyo García Galiano, <i>TIITBA a GUI for digital historical seismograms vectorization, analysis, and corrections: Case study of the 11/1/1928 (M6.3) Parral, Mexico earthquake,</i> <b>Joint Scientific Assembly IAGA-IASPEI 2021</b> India.</li>
	<li><b>Raul Daniel Corona Fernandez</b>, Miguel Angel Santoyo García Galiano, <i>TIITBA-HISVAC: A GUI FOR HISTORICAL SEISMOGRAMS VECTORIZATION, ANALYSIS AND CORRECTION,</i> <b>2020-UGM</b> Virtual Annual Meeting, Mexico.<a href="#Image1">[1]</a></li>
	<li><b>Raul Daniel Corona Fernandez</b>, Miguel Angel Santoyo García Galiano, <i>RE- ANÁLISIS DE GRANDES SISMOS HISTÓRICOS: RELOCALIZACIÓN DE LA SECUENCIA SÍSMICA DE 1928 EN OAXACA, MÉXICO, </i> <b>2020-UGM</b> Virtual Annual Meeting, Mexico.<a href="#Image2">[2]</a></li>
	<li>Miguel Angel Santoyo García Galiano, <b>Raul Daniel Corona Fernandez</b>, Masatoshi Miyazawa, Oscar Alberto Castro Artola, <i>“A BROADBAND SEISMOLOGICAL OBSERVATION NETWORK FOR SURFACE DYNAMIC DEFORMATIONS, NORTHWEST OF THE GUERRERO SEISMIC GAP: NEW RESULTS”</i>. <b>2019-UGM</b> Annual Meeting, Puerto Vallarta, Mexico.</li>
	<li><b>Corona-Fernandez R D.</b>, Santoyo M.A., <i>“TLALOC: A computer interface for analog seismograms analysis, digitization, and correction”,</i> <b>2019-IUGG General Assembly</b>, Montreal, Canada</li>
	<li><b>Corona-Fernandez R D.</b>, Santoyo M.A., <i>“GUI para digitalización y corrección de sismogramas históricos”,</i> <b>2018-UGM</b> Annual Meeting, Puerto Vallarta, Mexico</li>
	<li>Miguel Angel Santoyo García Galiano, <b>Raul Daniel Corona Fernandez</b>, Oscar Alberto Castro Artola, <i>“RED SISMOLÓGICA PARA LA OBSERVACIÓN DE ROTACIONES Y DEFORMACIONES DINÁMICAS POR SISMOS EN LA ZONA DE SUBDUCCIÓN MEXICANA: RESULTADOS PRELIMINARES”,</i> <b>2018-UGM</b> Annual Meeting, Puerto Vallarta, Mexico.</li>
	<li><b>Corona-Fernandez R D.</b>, Santoyo M.A, <i>“Estudio de fuente finita del sismo de Acambay el 19 de noviembre de 1912, con base en registros históricos”,</i> <b>2017-UGM</b> Annual Meeting, Puerto Vallarta, Mexico.</li>
	<li>Miguel Angel Santoyo García Galiano Vladimir Kostoglodov, <b>Raul Daniel Corona Fernandez</b> <i>“STRESS TRANSFER IN THE 3D MEXICAN SUBDUCTION ZONE DUE TO SLIP EVENTS”,</i> <b>2017-UGM</b> Annual Meeting, Puerto Vallarta, Mexico.</li>
	<li><b>Raul Daniel Corona Fernandez</b>, Miguel Angel Santoyo, <i>ANALYSIS OF THE ANALOG SEISMOGRAMS RECORDED DURING THE NOVEMBER 19, 1912 (M~7.0) ACAMBAY, CENTRAL MEXICO EARTHQUAKE: TOWARDS A FINITE SOURCE INVERSION,</i> <b>2017-IAG-IASPEI</b>, Kobe, Japan</li>
</ol>

---

---

<p id="Image1">[1] <img src="/img/sis-22-RAUGM2020.jpg" alt="SIS-22" width="700" height="400"></p> 
<p id="Image2">[2] <img src="/img/sis-19-RAUGM2020.png" alt="SIS-19" width="700" height="500"></p>
