+++
title = "Conference Proceeding chronology"
slug = "conferences"
+++

---

---

<ol type="1" reversed>
	<li><b>Raul Daniel Corona Fernandez</b>, Shri Krishna Singh, Miguel Angel Santoyo García Galiano, Arturo Iglesias, <i>A focal mechanism analysis for the 1928 Parral (M=6.3) and OAXACA (M=7.5) earthquake sequence, Mexico, based on analog smoked paper seismograms,</i> <b>LACSC-2022</b> Quito, Ecuador.</li>
	<li><b>Geosciences Center (CEGEO), UNAM. Institutional Seminar</b>.<\n><i>Old data for new knowledge: The use of analog seismograms to re-analyze large earthquakes in Mexico during the first half of the 20th century,</i> <b>January- 2022</b></li>
	<li><b>Raul Daniel Corona Fernandez</b>, Shri Krishna Singh, Miguel Angel Santoyo García Galiano, <i>Is the Acapulco earthquake of 8 september 2021 (mw 7.0) a repeat of the 11 May 1962 earthquake?,</i> <b>2021-UGM</b>Annual Meeting, Mexico.</li>
	<li><b>Raul Daniel Corona Fernandez</b>, Miguel Angel Santoyo García Galiano, <i>The 1928 Oaxaca, Mexico large-earthquakes sequence (M6.5-M7.8): preliminary results from the re-analysis of legacy seismograms and seismic bulletins information,</i> <b>37th General Assembly of the European Seismological Commission, 2021</b> Athens.</li>
	<li><b>Raul Daniel Corona Fernandez</b>, Miguel Angel Santoyo García Galiano, <i>TIITBA a GUI for digital historical seismograms vectorization, analysis, and corrections: Case study of the 11/1/1928 (M6.3) Parral, Mexico earthquake,</i> <b>Joint Scientific Assembly IAGA-IASPEI 2021</b> India.</li>
	<li><b>Raul Daniel Corona Fernandez</b>, Miguel Angel Santoyo García Galiano, <i>TIITBA-HISVAC: A GUI FOR HISTORICAL SEISMOGRAMS VECTORIZATION, ANALYSIS AND CORRECTION,</i> <b>2020-UGM</b> Virtual Annual Meeting, Mexico.<a href="#Image1">[1]</a></li>
	<li><b>Raul Daniel Corona Fernandez</b>, Miguel Angel Santoyo García Galiano, <i>RE- ANÁLISIS DE GRANDES SISMOS HISTÓRICOS: RELOCALIZACIÓN DE LA SECUENCIA SÍSMICA DE 1928 EN OAXACA, MÉXICO, </i> <b>2020-UGM</b> Virtual Annual Meeting, Mexico.<a href="#Image2">[2]</a></li>
	<li>Miguel Angel Santoyo García Galiano, <b>Raul Daniel Corona Fernandez</b>, Masatoshi Miyazawa, Oscar Alberto Castro Artola, <i>“A BROADBAND SEISMOLOGICAL OBSERVATION NETWORK FOR SURFACE DYNAMIC DEFORMATIONS, NORTHWEST OF THE GUERRERO SEISMIC GAP: NEW RESULTS”. <b>2019-UGM</b> Annual Meeting, Puerto Vallarta, Mexico.</li>
	<li><b>Corona-Fernandez R D.</b>, Santoyo M.A., <i>“TLALOC: A computer interface for analog seismograms analysis, digitization, and correction”,</i> <b>2019-IUGG General Assembly</b>, Montreal, Canada</li>
	<li><b>Corona-Fernandez R D.</b>, Santoyo M.A., <i>“GUI para digitalización y corrección de sismogramas históricos”,</i> <b>2018-UGM</b> Annual Meeting, Puerto Vallarta, Mexico</li>
	<li>Miguel Angel Santoyo García Galiano, <b>Raul Daniel Corona Fernandez</b>, Oscar Alberto Castro Artola, <i>“RED SISMOLÓGICA PARA LA OBSERVACIÓN DE ROTACIONES Y DEFORMACIONES DINÁMICAS POR SISMOS EN LA ZONA DE SUBDUCCIÓN MEXICANA: RESULTADOS PRELIMINARES”,</i> <b>2018-UGM</b> Annual Meeting, Puerto Vallarta, Mexico.</li>
	<li>Corona-Fernandez R D.</b>, Santoyo M.A, <i>“Estudio de fuente finita del sismo de Acambay el 19 de noviembre de 1912, con base en registros históricos”,</i> <b>2017-UGM</b> Annual Meeting, Puerto Vallarta, Mexico.</li>
	<li>Miguel Angel Santoyo García Galiano Vladimir Kostoglodov, <b>Raul Daniel Corona Fernandez</b> <i>“STRESS TRANSFER IN THE 3D MEXICAN SUBDUCTION ZONE DUE TO SLIP EVENTS”,</i> <b>2017-UGM</b> Annual Meeting, Puerto Vallarta, Mexico.</li>
	<li><b>Raul Daniel Corona Fernandez</b>, Miguel Angel Santoyo, <i>ANALYSIS OF THE ANALOG SEISMOGRAMS RECORDED DURING THE NOVEMBER 19, 1912 (M~7.0) ACAMBAY, CENTRAL MEXICO EARTHQUAKE: TOWARDS A FINITE SOURCE INVERSION,</i> <b>2017-IAG-IASPEI</b>, Kobe, Japan</li>
</ol>

---

---

<p id="Image1">[1] <img src="/img/sis-22-RAUGM2020.jpg" alt="SIS-22" width="700" height="400"></p> 
<p id="Image2">[2] <img src="/img/sis-19-RAUGM2020.png" alt="SIS-19" width="700" height="500"></p>
