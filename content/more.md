+++
title = "Resume"
+++

---

<!-- <iframe width='100%' height='100%' frameborder='0' src="/home/rdcorona/phD/Documentos/Personales/CV-EN-rdcorona-fernandez_2022Oct.pdf embedded=true"></iframe>
. -->

## Education

> **Ph.D. candidate in Earth Sciences** -_Seismology_- 2018-Present:   at National Autonomous University of Mexico, ***Institute of Geophysics***.

> **M.Sc. Earth Sciences – _Seismology_-** 2015 - 2018:  at at National Autonomous University of Mexico, ***Escuela Nacional de Estudios Superiores Morelia***

> **BSc. Geophysical Engineer** 2008 - 2012:   at National Autonomous University of Mexico, ***Engineer Faculty***

---

---

## Experience

<style type="text/css">
.tg  {border-collapse:collapse;border-color:#93a1a1;border-spacing:0;}
.tg td{background-color:#fdf6e3;border-bottom-width:1px;border-color:#93a1a1;border-style:solid;border-top-width:1px;
  border-width:0px;color:#002b36;font-family:Arial, sans-serif;font-size:14px;overflow:hidden;padding:10px 5px;
  word-break:normal;}
.tg th{background-color:#657b83;border-bottom-width:1px;border-color:#93a1a1;border-style:solid;border-top-width:1px;
  border-width:0px;color:#fdf6e3;font-family:Arial, sans-serif;font-size:14px;font-weight:normal;overflow:hidden;
  padding:10px 5px;word-break:normal;}
.tg .tg-7iv0{background-color:#eee8d5;border-color:#330001;font-size:20px;text-align:center;vertical-align:middle}
.tg .tg-scab{border-color:#330001;font-size:20px;font-weight:bold;text-align:left;vertical-align:middle}
.tg .tg-ze8y{border-color:#330001;font-family:Arial, Helvetica, sans-serif !important;font-size:20px;text-align:center;
  vertical-align:middle}
.tg .tg-gvo1{border-color:#330001;font-family:Arial, Helvetica, sans-serif !important;font-size:20px;font-weight:bold;
  text-align:center;vertical-align:middle}
.tg .tg-jfg9{border-color:#330001;text-align:center;vertical-align:middle}
.tg .tg-evfy{background-color:#eee8d5;border-color:#330001;font-size:20px;text-align:left;vertical-align:middle}
.tg .tg-2vsx{border-color:#330001;font-size:20px;font-weight:bold;text-align:center;vertical-align:middle}
.tg .tg-cqo4{border-color:#330001;font-size:20px;text-align:left;vertical-align:middle}
.tg .tg-kx5g{border-color:#330001;font-family:Arial, Helvetica, sans-serif !important;font-size:20px;text-align:left;
  vertical-align:middle}
.tg .tg-0yr2{border-color:#330001;font-size:20px;text-align:center;vertical-align:middle}
.tg .tg-exjp{border-color:#330001;text-align:left;vertical-align:middle}
.tg .tg-8682{background-color:#eee8d5;border-color:#330001;font-family:Arial, Helvetica, sans-serif !important;font-size:20px;
  text-align:left;vertical-align:middle}
.tg .tg-9ek4{background-color:#eee8d5;border-color:#330001;font-family:Arial, Helvetica, sans-serif !important;font-size:20px;
  text-align:center;vertical-align:middle}
</style>
<table class="tg">
<thead>
  <tr>
    <th class="tg-gvo1">Year</th>
    <th class="tg-2vsx">Position</th>
    <th class="tg-2vsx">Company / Institution</th>
    <th class="tg-scab">Responsibilities</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td class="tg-evfy">Jan 2018-Present</td>
    <td class="tg-7iv0">Research Assistant</td>
    <td class="tg-7iv0">Insitute of Geophysics, UNAM</td>
    <td class="tg-evfy">•Python developer of API’s and GUI for analyses, processing, and interpretation of seismic data<br>•UNIX server’s administrator<br>•Development of Scientific Research projects, publication of scientific articles and presentation at international scientific conferences</td>
  </tr>
  <tr>
    <td class="tg-cqo4"></td>
    <td class="tg-0yr2"></td>
    <td class="tg-0yr2"></td>
    <td class="tg-cqo4"></td>
  </tr>
  <tr>
    <td class="tg-evfy">Jan-Dec 2022</td>
    <td class="tg-7iv0">External consultant</td>
    <td class="tg-7iv0">SELOME</td>
    <td class="tg-evfy">• Probabilistic assessment of seismic hazard (PASH) for the Federal Highway Network in Mexico.<br>•Identification of the current regions of greatest danger for the Federal Highway Network and in the future.</td>
  </tr>
  <tr>
    <td class="tg-cqo4"></td>
    <td class="tg-0yr2"></td>
    <td class="tg-0yr2"></td>
    <td class="tg-cqo4"></td>
  </tr>
  <tr>
    <td class="tg-evfy">Jan 2018-Present</td>
    <td class="tg-7iv0">Research projects collaborator</td>
    <td class="tg-7iv0">Different</td>
    <td class="tg-evfy">• Project to study seismic metamaterials in the context of seismic engineering at the UNAM Institute of Engineering and Texas University.<br>• Repeating Large Earthquakes (M≥7.0) along the Mexican Subduction Zone. A qualitative and quantitative waveform comparison for different historical and relatively recent earthquakes in Mexico, to establish repetition and to estimate return periods.<br>• Project CONACYT PN-15-639 To calculate dynamic deformations with a nearby station arrangement.Installation and maintenance of a semi-permanent 3 seismic stations network with Trillium 120 Posthole instruments in Zihuatanejo, Guerrero airport</td>
  </tr>
  <tr>
    <td class="tg-exjp">.</td>
    <td class="tg-jfg9">.</td>
    <td class="tg-jfg9">.</td>
    <td class="tg-exjp">.</td>
  </tr>
  <tr>
    <td class="tg-8682">May-Jun 2015</td>
    <td class="tg-9ek4">Field engineer</td>
    <td class="tg-9ek4">Terratunel</td>
    <td class="tg-8682">•Shallow geophysical exploration SEV, Seismic Reflection Tomography (SRT) and ground penetrating radar (GPR) data acquisition SEV, SRT and GPR data interpretation</td>
  </tr>
  <tr>
    <td class="tg-kx5g"></td>
    <td class="tg-ze8y"></td>
    <td class="tg-ze8y"></td>
    <td class="tg-kx5g"></td>
  </tr>
  <tr>
    <td class="tg-8682">Sep 2014 - Jan 2015</td>
    <td class="tg-9ek4">Field engineer</td>
    <td class="tg-9ek4">Geoambiente-Sacmag</td>
    <td class="tg-8682">• Shallow geophysical exploration<br>•SEV, Seismic Reflection Tomography (SRT) and ground penetrating radar (GPR) data acquisition<br>• SEV, SRT and GPR data interpretation</td>
  </tr>
  <tr>
    <td class="tg-kx5g"></td>
    <td class="tg-ze8y"></td>
    <td class="tg-ze8y"></td>
    <td class="tg-kx5g"></td>
  </tr>
  <tr>
    <td class="tg-8682">Jan-Sept 2014</td>
    <td class="tg-9ek4">Engineer Jr.</td>
    <td class="tg-9ek4">COMESA</td>
    <td class="tg-8682">•Seismic Explorations brigade<br>•Seismic Refraction 3D model<br>•Processing and interpretation of Seismic Refraction 3D data for oil fields<br>•Field logistics, management and planning</td>
  </tr>
</tbody>
</table>


<!-- | Year | Position | Company / Institution | Responsibilities |
| :-------------  | :-------------  | :---------------------------  | :--------------------------------- |
|  Jan 2018-Present | Research Assistant | Insitute of Geophysics, UNAM | •Python developer of API’s and GUI for analyses, processing, and interpretation of seismic data<br /> •UNIX server’s administrator <br /> •Development of Scientific Research projects, publication of scientific articles and presentation at international scientific conferences |
|.| . | .| .|  
| Jan-Dec 2022  | External consultant | SELOME  |• Probabilistic assessment of seismic hazard (PASH) for the Federal Highway Network in Mexico.<br/>•Identification of the current regions of greatest danger for the Federal Highway Network and in the future. |
|.| . | .| .|
| Jan 2018-Present | Research projects collaborator | Different | • Project to study seismic metamaterials in the context of seismic engineering at the UNAM Institute of Engineering and Texas University.<br/>• Repeating Large Earthquakes (M≥7.0) along the Mexican Subduction Zone. A qualitative and quantitative waveform comparison for different historical and relatively recent earthquakes in Mexico, to establish repetition and to estimate return periods.<br/> • Project CONACYT PN-15-639 To calculate dynamic deformations with a nearby station arrangement.Installation and maintenance of a semi-permanent 3 seismic stations network with Trillium 120 Posthole instruments in Zihuatanejo, Guerrero airport |
|.| . | .| .|
| May-Jun 2015  | Field engineer | Terratunel  | •Shallow geophysical exploration SEV, Seismic Reflection Tomography (SRT) and ground penetrating radar (GPR) data acquisition SEV, SRT and GPR data interpretation |
|.| . | .| .|
| Sep 2014 - Jan 2015 | Field engineer | Geoambiente-Sacmag  | • Shallow geophysical exploration<br /> •SEV, Seismic Reflection Tomography (SRT) and ground penetrating radar (GPR) data acquisition <br /> • SEV, SRT and GPR data interpretation |
|.| . | .| .|
| Jan-Sept  2014  | Engineer Jr.  | COMESA   | •Seismic Explorations brigade<br /> •Seismic Refraction 3D model <br />•Processing and interpretation of Seismic Refraction 3D data for oil fields <br />•Field logistics, management and planning | -->

---

---

## Teaching experience

Associate lecture professor at the **NATIONAL AUTONOMOUS UNIVERSITY OF MEXICO (UNAM)**

- **2022 Summer-Fall** : Lecture -> Applied Geophysics II, [ENCiT](https://twitter.com/ENCiT_UNAM), ***UNAM***
- **2022 Summer-Fall** : Lecture -> Earth Science Programming I, [Sciences Faculty](https://twitter.com/fciencias), ***UNAM***
- **2022 Winter-Spring** : Lecture -> Geodynamics, [ENCiT](https://twitter.com/ENCiT_UNAM), ***UNAM***
- **2022 Winter-Spring** : Lecture -> Earth Science Programming II, [Sciences Faculty](https://twitter.com/fciencias), ***UNAM***
- **2021 Summer-Fall** :   Lecture -> Earth Science Programming I, [Sciences Faculty](https://twitter.com/fciencias), ***UNAM***
- **2021 Winter-Spring**:  Lecture -> Earth Science Programming I, [Sciences Faculty](https://twitter.com/fciencias), ***UNAM***
- **2019 Summer-Fall**:    Lecture -> Computational analysis of geophysical data, [Sciences Faculty](https://twitter.com/fciencias), ***UNAM***
- **2018 both semesters**: Lecture -> Analysis and processing of time series, ENES Morelia, ***UNAM***

---

---

## [Publications](../publications)

## [Conference and seminars proceeding](../conferences)

---

---

## Skills:

+ Technical skills

  - Linix-Unix servers administrator
  - More than 5 years experience on scientific programing and ad-hoc code develop to solve mathematical-physical problems. (Inversion methodologies,database processing, mapping.)
  - Processing, debugging and analysis of seismic databases.
  - Obtaining and processing specialized seismic data from various seismological database agencies.
  - Several software managements and ***Programming +***
  - Analytic skills in solving physical-mathematical problems.
+ Social skills

  - Initiative and willing to learn and provide my knowledge
  - Strong oral and written communication in English and Spanish
  - Fluent in spoken and written English (B2)
  - Coordination of working groups
  - Leadership
  - Work in a team
+ Computational and Programming skills

<style type="text/css">
.tg  {border-collapse:collapse;border-color:black;border-spacing:0;border-style:solid;border-width:1px;}
.tg td{border-style:solid;border-width:0px;font-family:Arial, sans-serif;font-size:14px;overflow:hidden;
  padding:10px 5px;word-break:normal;}
.tg th{border-style:solid;border-width:0px;font-family:Arial, sans-serif;font-size:14px;font-weight:normal;
  overflow:hidden;padding:10px 5px;word-break:normal;}
.tg .tg-zkl2{border-color:inherit;font-size:20px;font-weight:bold;text-align:left;vertical-align:top}
.tg .tg-eqm3{border-color:inherit;font-size:20px;text-align:left;vertical-align:top}
</style>

<table class="tg">
<thead>
  <tr>
    <th class="tg-zkl2">Scientific<br>Programing   </th>
    <th class="tg-zkl2">Seismology   </th>
    <th class="tg-zkl2">GIS and Mapping  </th>
    <th class="tg-zkl2">Developer Tools<br>and Framework    </th>
    <th class="tg-zkl2">Misc</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td class="tg-eqm3">Python3</td>
    <td class="tg-eqm3">R-CRISIS</td>
    <td class="tg-eqm3">QGIS</td>
    <td class="tg-eqm3">Django (basic)</td>
    <td class="tg-eqm3">Linux - Unix</td>
  </tr>
  <tr>
    <td class="tg-eqm3">Bash </td>
    <td class="tg-eqm3">SAC</td>
    <td class="tg-eqm3">ArcGIS</td>
    <td class="tg-eqm3">Hugo</td>
    <td class="tg-eqm3">Adobe Tools</td>
  </tr>
  <tr>
    <td class="tg-eqm3">MaLab</td>
    <td class="tg-eqm3">SEISAN</td>
    <td class="tg-eqm3">GMT</td>
    <td class="tg-eqm3">MarkDown</td>
    <td class="tg-eqm3">Office Tools</td>
  </tr>
  <tr>
    <td class="tg-eqm3">Fortran</td>
    <td class="tg-eqm3">Obspy</td>
    <td class="tg-eqm3">PyGMT</td>
    <td class="tg-eqm3"></td>
    <td class="tg-eqm3">InkScape</td>
  </tr>
  <tr>
    <td class="tg-eqm3">C#</td>
    <td class="tg-eqm3">Dectra</td>
    <td class="tg-eqm3"></td>
    <td class="tg-eqm3"></td>
    <td class="tg-eqm3">Gimp</td>
  </tr>
</tbody>
</table>

---

---

## Continuing Education

- SpecFem Workshop october 5-7 2022| [x]

> 3-day Workshop to learn about thue use of SpecFEm 2D y 3D. From U Alaska, U. Toronto and U. South Florida

- MTUQ Workshop April 27-28 2022| [x]

> 2-day Workshop to learn about Moment Tensor Uncertainty Quantification inversion code. From U Alaska, U. Toronto and U. South Florida

- Academic Writing September 2020| [x]

> 2-weeks Workshop for Writing and Publishing Academic Articles in English. From graduate UNAM

- ROSES Summer 2020 [x]

> American Geophysical Union Seismology Section Summer School:[Remote Online Sessions for Emerging Seismologists](https://www.iris.edu/hq/inclass/course/roses)
> Participated in a 11-week workshop focused on online learning in seismology. Workshop offered Python-based tutorials in data processing, modeling and interpretation.

- Python3 Summer 2018 [x]

> 3-weeks workshop focused on scientific Python3 for scientific data processing and graphical representation

- Seismic Data Analysis Jan 2015| [x]

> National Seismological Service (Mexico): SeismicData Analysis.
> Two modules course (basic and advanced) focused on locating, calculating the focal mechanism and the magnitude of Mexican earthquakes with SEISAN

---

---

## Contribution to the Scientific community

### Symposium co-organizer:

> Preliminary results of the earthquake of September 7,2021 Mw7.1 in Acapulco, Guerrero. Annual Meeting of the Mexican Geophysical Union, 2021.

### **Since 2021**  Co-organizer of _TierraFest_

> An Earth science festival organized by organized by *Planeteando*, *Seismology is the Wave*, and different earth sciences Research Institutes.
> [Wikicarrera](https://seismologyisthewave.gitlab.io/siw-web-site/post/tierrafest/wiki_carrera_video/) .

### **Since 2020**  Science communicator with _Seismology is the wave_

> At [Seismology is the wave](https://seismologyisthewave.gitlab.io/siw-web-site/) we do scientific dissemination through small texts, infographics, and even humorous figures (memes). All with seismology content.

### **Since 2019** Science communicator with _Planeteando_.

> [Planeteando](https://planeteando.org/) is a blog where you will discover the coolest things on Planet Earth.
> Collaborators write content about the "spheres" that make up our Planet: biosphere, cryosphere, atmosphere, oceans, solid earth, economy and society!

### **2018 & 2019** UGM - Staff

> Staff at the annual meeting of the UGM (Geophysical Mexican Union)
